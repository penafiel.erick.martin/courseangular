export interface Passenger {
  name: string
  children?: string[]

}

const passenger1: Passenger = {
  name: 'John'
}

const passenger2: Passenger = {
  name: 'Melina',
  children: ['John Jr', 'Jane']
}

const printChildren = (passenger: Passenger) => {
  const howManyChildren = passenger.children?.length || 0

  console.log(passenger.name, howManyChildren)
}

printChildren(passenger1)

