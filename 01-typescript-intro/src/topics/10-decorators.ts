function classDecorator(constructor: any) {
  return new class extends constructor {
    newProperty = "New property"
    hello = "override"
  }
}


// @classDecorator
export class SuperClass {
  public myProperty: string = 'SuperClass'
  print() {
    console.log('SuperClass')
  }
}

console.log(SuperClass)

const myClass = new SuperClass()
console.log(myClass)