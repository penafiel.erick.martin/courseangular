import { Component, Input } from '@angular/core';
import { Country } from '../../interfaces/countries.interface';

@Component({
  selector: 'countries-table',
  templateUrl: './country-table.component.html',
  styles: [
    `img {
      width: 30px;
      height: 30px;
      border-radius: 50%;
    }`
  ]
})
export class CountryTableComponent {
  @Input()
  countries: Country[] = []
}
