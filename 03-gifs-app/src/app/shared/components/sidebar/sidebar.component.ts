import { Component } from '@angular/core';
import { GifsService } from '../../../services/gifs.service';

@Component({
  selector: 'shared-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.css'
})
export class SidebarComponent {
  constructor(private gifsServices: GifsService) { }

  get history() {
    return this.gifsServices.tagsHistory
  }

  public searchTag(tag: string) {
    return this.gifsServices.searchTag(tag)
  }
}
