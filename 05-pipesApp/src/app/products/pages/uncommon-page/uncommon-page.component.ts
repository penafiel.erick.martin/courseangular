import { Component } from '@angular/core';
import { interval, tap } from 'rxjs';

@Component({
  selector: 'app-uncommon-page',
  templateUrl: './uncommon-page.component.html',
  styleUrl: './uncommon-page.component.css'
})
export class UncommonPageComponent {
  //i18n Select
  public name: string = 'Juan';
  public gender: 'male' | 'female' = 'male'

  public invitationMap = {
    male: 'invitarlo',
    female: 'invitarla'
  }

  changeClient() {
    this.name = "Melisa"
    this.gender = "female"
  }

  //i18n Plural
  public clients: string[] = ['Juan', 'Pedro', 'Luis', 'Ana', 'Melisa', 'Laura', 'Fernando', 'Carlos', 'María', 'José']
  public clientsMap = {
    '=0': 'no tenemos ningún cliente esperando',
    '=1': 'tenemos 1 cliente esperando',
    'other': 'tenemos # clientes esperando'
  }

  public deleteClient() {
    this.clients.shift()
  }

  //KeyValue Pipe
  public person = {
    name: 'Juan',
    age: 30,
    address: 'Calle 123 # 456'
  }

  //Async Pipe
  public myObservableTimer = interval(2000).pipe(
    tap((value) => console.log("Tap: ", value))
  )

  // No se puede cancelar una promesa con el async pipe
  public myPromise = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('Promesa resulta')
      console.log("Promesa resulta")
    }, 3500)
  })
}
