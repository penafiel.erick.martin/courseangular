import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanMatch, GuardResult, MaybeAsync, Route, Router, RouterStateSnapshot, UrlSegment } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { map, Observable, tap } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class PublicGuard implements CanActivate, CanMatch {
  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  canMatch(route: Route, segments: UrlSegment[]): Observable<boolean> {
    return this.authService.checkAuthentication()
      .pipe(
        tap(isAuthenticated => console.log('isAuthenticated', isAuthenticated)),
        tap(isAuthenticated => {
          if (isAuthenticated) this.router.navigate(['/heroes/list'])
        }),
        map(isAuthenticated => !isAuthenticated)
      )
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.authService.checkAuthentication()
      .pipe(
        tap(isAuthenticated => console.log('isAuthenticated', isAuthenticated)),
        tap(isAuthenticated => {
          if (isAuthenticated) this.router.navigate(['/heroes/list'])
        }),
        map(isAuthenticated => !isAuthenticated)
      )
  }
}