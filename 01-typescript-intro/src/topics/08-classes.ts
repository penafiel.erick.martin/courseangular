export class Person {
  constructor(public name: string, address?: string) { }

  // constructor() {
  //   this.name = 'John Doe'
  //   this.address = '123 Main St'
  // }

}

// export class Hero extends Person {
//   constructor(public alterEgo: string, age: number, realName: string) {
//     super(realName, 'New York')
//   }
// }

export class Hero {
  constructor(public alterEgo: string, public age: number, public realName: string, public person: Person) { }
}


const tony = new Person('Tony Stark')
const ironMan = new Hero('Iron Man', 40, 'Tony Stark', tony)

console.log(ironMan)