import { Component, Input, OnInit } from '@angular/core';
import { Gif } from '../../interfaces/gifs.interfaces';

@Component({
  selector: 'gifs-card-gifs',
  templateUrl: './card-gifs.component.html',
  styleUrl: './card-gifs.component.css'
})
export class CardGifsComponent implements OnInit {
  constructor() { }
  ngOnInit(): void {
    if (!this.gif) throw new Error('Gif is required')
  }

  @Input()
  public gif!: Gif
}
