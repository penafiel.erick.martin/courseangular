import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, count, delay, map, Observable, of, tap } from 'rxjs';
import { Country } from '../interfaces/countries.interface';
import { CacheStore } from '../interfaces/cache-store.interface';
import { Region } from '../interfaces/region.type';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {
  private API_URL = "https://restcountries.com/v3.1"
  public cacheStore: CacheStore = {
    byCapital: { term: '', countries: [] },
    byCountries: { term: '', countries: [] },
    byRegion: { region: '', countries: [] }
  }

  constructor(private httpClient: HttpClient) {
    this.loadLocalStorage()
  }

  private saveLocalStorage() {
    localStorage.setItem("cacheStore", JSON.stringify(this.cacheStore))
  }

  private loadLocalStorage() {
    if (!localStorage.getItem("cacheStore")) return

    this.cacheStore = JSON.parse(localStorage.getItem("cacheStore")!)
  }

  getCountriesRequest(url: string): Observable<Country[]> {
    return this.httpClient.get<Country[]>(url)
      .pipe(
        catchError((error) => of([])),
      )
  }

  searchCapital(term: string): Observable<Country[]> {
    return this.getCountriesRequest(`${this.API_URL}/capital/${term}`)
      .pipe(
        tap(countries => this.cacheStore.byCapital = { term, countries }),
        tap(() => this.saveLocalStorage())
      )
  }

  searchCountry(term: string): Observable<Country[]> {
    return this.getCountriesRequest(`${this.API_URL}/name/${term}`)
      .pipe(
        tap(countries => this.cacheStore.byCountries = { term, countries }),
        tap(() => this.saveLocalStorage())
      )
  }

  searchRegion(region: Region): Observable<Country[]> {
    return this.getCountriesRequest(`${this.API_URL}/region/${region}`)
      .pipe(
        tap(countries => this.cacheStore.byRegion = { region, countries }),
        tap(() => this.saveLocalStorage())
      )
  }

  searchCountryByAlpha(id: string): Observable<Country | null> {
    return this.httpClient.get<Country[]>(`${this.API_URL}/alpha/${id}`)
      .pipe(
        map(countries => countries[0] || null),
        catchError(error => {
          return of(null)
        })
      )
  }
}
