import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-basic-page',
  templateUrl: './basic-page.component.html',
  styles: ``
})
export class BasicPageComponent {
  // public myForm: FormGroup = new FormGroup({
  //   name: ['',][], []), //('', validaciones sincronas, validaciones asincronas)
  //   price: new FormControl(0), //('', validaciones sincronas, validaciones asincronas)
  //   inStorage: new FormControl(0), //('', validaciones sincronas, validaciones asincronas)
  // })

  constructor(
    private fb: FormBuilder
  ) { }

  public myForm: FormGroup = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(3)], []], //('', validaciones sincronas, validaciones asincronas)
    price: [0, [Validators.required, Validators.min(0)]], //('', validaciones sincronas, validaciones asincronas)
    inStorage: [0, [Validators.required, Validators.min(0)]], //('', validaciones sincronas, validaciones asincronas)  
  })

  isValid(field: string) {
    return this.myForm.controls[field].touched && this.myForm.controls[field].errors
  }

  getFieldError(field: string): string | null {
    if (!this.myForm.controls[field].errors) return null

    const errors = this.myForm.controls[field].errors || {}

    for (const key of Object.keys(errors)) {
      switch (key) {
        case 'required':
          return 'Este campo es requerido'
        case 'minlength':
          return `Este campo necesita minimo ${errors[key].requiredLength} caracteres`
      }
    }

    return null
  }

  onSave(): void {
    if (!this.myForm.valid) {
      this.myForm.markAllAsTouched()
      return
    }
    console.log(this.myForm.value);

    this.myForm.reset({
      name: '',
      price: 0,
      inStorage: 0
    });
  }
}
