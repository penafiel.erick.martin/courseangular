export enum Color {
  red, green, blue, yellow, white, black
}

export interface Hero {
  name: string
  canFly: boolean
  color: Color
}