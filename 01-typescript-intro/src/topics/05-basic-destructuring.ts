interface AudioPlayer {
  audioVolume: number
  songDuration: number
  song: string
  details: Details
}

interface Details {
  author: string
  year: number
}

export const audioPlayer: AudioPlayer = {
  audioVolume: 50,
  songDuration: 3.5,
  song: 'The song',
  details: {
    author: 'The author',
    year: 2021
  }
}

// const { audioVolume, song, details } = audioPlayer
// const { author } = details

// console.log({ audioVolume, song, author })

// const personajes = ['Goku', 'Vegeta', 'Trunks']
const [, , trunks] = ['Goku', 'Vegeta', 'Trunks']
console.log(trunks)


export { }