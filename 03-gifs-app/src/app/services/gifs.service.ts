import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Gif, SearchResponse } from '../gifs/interfaces/gifs.interfaces';

// const API_KEY: string = "nhyhl4IaWBY4VneXcYEVwYBQftS69lKH"

@Injectable({
  providedIn: 'root'
})
export class GifsService {
  public gifsList: Gif[] = []

  // private _tagsHistory: string[] = localStorage.getItem('tags') ? JSON.parse(localStorage.getItem('tags')!) : []
  private _tagsHistory: string[] = []
  private API_KEY: string = "nhyhl4IaWBY4VneXcYEVwYBQftS69lKH"
  private serviceUrl: string = "https://api.giphy.com/v1/gifs"

  constructor(private http: HttpClient) {
    this.loadLocalStorage()
    this.loadGifs()
  }

  get tagsHistory() {
    return [...this._tagsHistory]
  }

  private organizeHistory(tag: string) {
    tag = tag.toLowerCase()

    if (this._tagsHistory.includes(tag)) {
      this._tagsHistory = this._tagsHistory.filter(t => t !== tag)
    }
    this._tagsHistory.unshift(tag)
    this._tagsHistory = this._tagsHistory.splice(0, 10)
    this.saveLocalStorage()
  }

  private saveLocalStorage(): void {
    localStorage.setItem('history', JSON.stringify(this._tagsHistory))
  }

  private loadLocalStorage(): void {
    if (!localStorage.getItem('history')) return;
    this._tagsHistory = JSON.parse(localStorage.getItem('history')!)
  }

  private loadGifs() {
    const lastSearch = JSON.parse(localStorage.getItem('history')!)[0]
    if (!lastSearch) return
    this.searchTag(lastSearch)
  }

  public searchTag(tag: string) {
    if (tag.length === 0) return
    this.organizeHistory(tag)

    // fetch(`https://api.giphy.com/v1/gifs/search?api_key=nhyhl4IaWBY4VneXcYEVwYBQftS69lKH&q=panda&limit=25&rating=g&lang=en`)
    //   .then(response => response.json())
    //   .then(data => console.log(data))

    const params = new HttpParams()
      .set('api_key', this.API_KEY)
      .set('q', tag)
      .set('limit', '25')

    this.http.get<SearchResponse>(`${this.serviceUrl}/search`, { params })
      .subscribe((response) => {
        this.gifsList = response.data
      })
  }
}
