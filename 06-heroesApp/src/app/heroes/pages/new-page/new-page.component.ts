import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Hero, Publisher } from '../../interfaces/hero.interfaces';
import { HeroService } from '../../services/hero.service';
import { ActivatedRoute, Router } from '@angular/router';
import { filter, switchMap } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../../components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-new-page',
  templateUrl: './new-page.component.html',
  styles: ``
})
export class NewPageComponent implements OnInit {
  public heroForm = new FormGroup({
    id: new FormControl(''),
    superhero: new FormControl(''),
    publisher: new FormControl<Publisher>(Publisher.DCComics),
    alter_ego: new FormControl(''),
    first_appearance: new FormControl(''),
    characters: new FormControl(''),
    alt_img: new FormControl('')
  })

  public publishers = [
    {
      id: 'DC Comics',
      name: 'DC Comics'
    },
    {
      id: 'Marvel Comics',
      name: 'Marvel Comics'
    }
  ]

  constructor(
    private heroService: HeroService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private snackbar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    if (!this.router.url.includes('edit')) return

    this.activatedRoute.params
      .pipe(
        switchMap(({ id }) => this.heroService.getHeroById(id))
      )
      .subscribe((hero) => {
        if (!hero) return this.router.navigateByUrl('/')
        this.heroForm.reset(hero)
        return
      })
  }

  get currentHero(): Hero {
    const hero = this.heroForm.value as Hero
    return hero
  }

  onSubmit() {
    if (this.heroForm.invalid) return

    if (this.currentHero.id) {
      this.heroService.updateHero(this.currentHero)
        .subscribe(hero => {
          this.showSnackbar(`${hero.superhero} updated`)
        })

      return
    }

    this.heroService.addHero(this.currentHero)
      .subscribe(hero => {
        this.router.navigate(['/heroes/edit', hero.id])
        this.showSnackbar(`${hero.superhero} created`)
      })
  }

  onDeleteHero() {
    if (!this.currentHero.id) throw new Error('Hero id is required')

    const dialog = this.dialog.open(ConfirmDialogComponent, {
      data: this.currentHero.superhero
    })

    dialog.afterClosed()
      .pipe(
        filter((result: boolean) => result),
        switchMap(() => this.heroService.deleteHero(this.currentHero)),
        filter((wasDeleted: boolean) => wasDeleted)
      )
      .subscribe(() => {
        this.router.navigate(['/heroes'])
      })

    // dialog.afterClosed()
    //   .subscribe((result) => {
    //     if (!result) return

    //     this.heroService.deleteHero(this.currentHero)
    //       .subscribe(wasDeleted => {
    //         if (wasDeleted) this.router.navigate(['/heroes'])
    //       })
    //   })
  }

  showSnackbar(message: string): void {
    this.snackbar.open(message, 'Done', {
      duration: 2500
    })
  }
}
