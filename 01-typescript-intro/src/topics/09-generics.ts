export function whatsMyType<T>(argument: T): T {
  return argument
}

// const amIString = whatsMyType('Hello')
// const amINumber = whatsMyType(100)
// const amIArray = whatsMyType([1, 2, 3])

const amIString = whatsMyType<string>('Hello')
const amINumber = whatsMyType<number>(100)
const amIArray = whatsMyType<number[]>([1, 2, 3])

console.log(amIString.split(" "))
console.log(amINumber + 100)
console.log(amIArray.map((value) => value * 2))